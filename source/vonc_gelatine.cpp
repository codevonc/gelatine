#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include "c4d.h"
#include "c4d_symbols.h"
#include "main.h"
#include "Ovoncgelatine.h"
using namespace std;

#define MODULE_ID 1037749
#define PRISES 2

struct Cache {
	vector<Vector> precAbs;
	vector<Vector> elasticite;
	vector<Float> facteur;
	Float secPrec;
};

class VoncGelatine : public ObjectData
{
private:
	const String VERSION = "1.0";

	map<String, Cache> pointsCache;
	Int32 tpsPrec = -1;
	Float ansTps = -1;

	Int32 mode = VONC_GELATINE_MODE_SRC;
	Float rayon = 50.;
	Float rayonMax = 300.;
	Float elasticite = .5;
	Float intensite = 1.;
	Float amortis = 0.;
	Float force = 0.;
	Bool inverser = false;
	String selection = "";
	Float angle = 0.;

	Float ansRayon = -1.;
	Float ansRayonMax = -1;
	Int32 ansMode = -1;
	String ansSelection = "-1";

	SplineData* courbe;

	static void DrawCercle(BaseDraw* bd, BaseDrawHelp* bh, Float rayon, Vector couleur);
	static Vector MixVec(Vector v1, Vector v2, Float t);
	static String ObjID(BaseObject* op);

	void RecupDepart(BaseObject* op, Matrix op_mg, Matrix mod_mg, vector<Vector>* departs);
	vector<Float> CalculeFacteur(PointObject* op, Matrix op_mg, Matrix mod_mg);
	vector<Vector> CalculeNormales(PolygonObject* op, Matrix op_mg);
	void Libere(BaseObject* mod);
	void Initialise(PointObject* op, Matrix op_mg, Matrix mod_mg, String id);
	void RendUniqueOpsIter(BaseObject* obj, vector<Int32>* ips);
	void RendUniqueOps(BaseObject* mod);
	Bool DoitInit(BaseObject* op, String id);
	Bool DoitLiberer(BaseTime* time, BaseDocument* doc);
	void InitCourbe(BaseContainer* donnees);

public:
	virtual Bool Init				(GeListNode* node);
	virtual void Recup				(BaseObject* op);
	virtual void CheckDirty			(BaseObject* op, BaseDocument* doc);
	virtual Bool Message			(GeListNode* node, Int32 type, void* data);
	virtual Bool GetDEnabling		(GeListNode* node, const DescID &id, const GeData &t_data, DESCFLAGS_ENABLE flags, const BaseContainer* itemdesc);
	virtual void GetDimension		(BaseObject* op, Vector* mp, Vector* rad);
	virtual DRAWRESULT Draw			(BaseObject* op, DRAWPASS type, BaseDraw* bd, BaseDrawHelp* bh);
	virtual void GetHandle			(BaseObject* op, Int32 i, HandleInfo& info);
	virtual Int32 DetectHandle		(BaseObject* op, BaseDraw* bd, Int32 x, Int32 y, QUALIFIER qualifier);
	virtual Bool MoveHandle			(BaseObject* op, BaseObject* undo, const Vector& mouse_pos, Int32 hit_id, QUALIFIER qualifier, BaseDraw* bd);
	virtual Bool ModifyObject		(BaseObject* op, BaseDocument* doc, BaseObject* mod, const Matrix& op_mg, const Matrix& mod_mg, Float lod, Int32 flags, BaseThread* thread);

	static NodeData* Alloc() { return NewObjClear(VoncGelatine); }
};

Vector VoncGelatine::MixVec(Vector v1, Vector v2, Float t) {
	return v1 + (v2 - v1) * t;
}

void VoncGelatine::GetDimension(BaseObject* op, Vector* mp, Vector* rad)
{
	BaseContainer* data = op->GetDataInstance();
	*mp	 = Vector(0.0);
	*rad = Vector(data->GetFloat(VONC_GELATINE_RAYON_MAX));
}

void VoncGelatine::GetHandle(BaseObject* op, Int32 i, HandleInfo& info)
{
	BaseContainer* data = op->GetDataInstance();
	if (!data)
		return;

	switch (i)
	{
		case 0 :
			info.position.x = data->GetFloat(VONC_GELATINE_RAYON);
			info.direction.x = 1.0;
			info.type = HANDLECONSTRAINTTYPE::LINEAR;
			break;
		case 1 :
			info.position.x = data->GetFloat(VONC_GELATINE_RAYON_MAX);
			info.direction.x = 1.0;
			info.type = HANDLECONSTRAINTTYPE::LINEAR;
			break;
		default :
			break;
	}
}

Int32 VoncGelatine::DetectHandle(BaseObject* op, BaseDraw* bd, Int32 x, Int32 y, QUALIFIER qualifier)
{
	if (qualifier & QUALIFIER::CTRL)
		return NOTOK;

	HandleInfo   info;
	Matrix		 mg = op->GetMg();
	Int32		 i, ret = NOTOK;
	Vector		 p;

	for (i = 0; i < PRISES; i++)
	{
		GetHandle(op, i, info);
		if (bd->PointInRange(mg * info.position, x, y))
		{
			ret = i;
			if (!(qualifier & QUALIFIER::SHIFT))
				break;
		}
	}
	return ret;
}

Bool VoncGelatine::MoveHandle(BaseObject* op, BaseObject* undo, const Vector& mouse_pos, Int32 hit_id, QUALIFIER qualifier, BaseDraw* bd)
{
	BaseContainer* dst = op->GetDataInstance();

	HandleInfo info;
	Float val = mouse_pos.x;
	GetHandle(op, hit_id, info);

	if (bd)
	{
		Matrix mg	 = op->GetUpMg() * undo->GetMl();
		Vector pos = bd->ProjectPointOnLine(mg * info.position, mg.sqmat * info.direction, mouse_pos.x, mouse_pos.y);
		val = Dot(~mg * pos, info.direction);
	}

	switch (hit_id)
	{
		case 0:
			dst->SetFloat(VONC_GELATINE_RAYON, ClampValue(val, (Float) 0.0, (Float)MAXRANGE));
			break;

		case 1:
			dst->SetFloat(VONC_GELATINE_RAYON_MAX, ClampValue(val, (Float) 0.0, (Float)MAXRANGE));
			break;

		default:
			break;
	}
	return true;
}

void VoncGelatine::DrawCercle(BaseDraw* bd, BaseDrawHelp* bh, Float rayon, Vector couleur) {
	Matrix m = bh->GetMg();
	Vector h;
	m.sqmat *= rayon;
	bd->SetMatrix_Matrix(nullptr, Matrix());
	bd->SetPen(couleur);
	bd->DrawCircle(m);
	h = m.sqmat.v2; m.sqmat.v2 = m.sqmat.v3; m.sqmat.v3 = h;
	bd->DrawCircle(m);
	h = m.sqmat.v1; m.sqmat.v1 = m.sqmat.v3; m.sqmat.v3 = h;
	bd->DrawCircle(m);
}

DRAWRESULT VoncGelatine::Draw(BaseObject* op, DRAWPASS drawpass, BaseDraw* bd, BaseDrawHelp* bh)
{
	if (drawpass == DRAWPASS::OBJECT)
	{
		BaseContainer* donnees = op->GetDataInstance();
		Int32 mode = donnees->GetInt32(VONC_GELATINE_MODE);

		if (mode == VONC_GELATINE_MODE_SRC)
			DrawCercle(bd, bh, donnees->GetFloat(VONC_GELATINE_RAYON), Vector(.1, 1., 0.5));

		if (mode != VONC_GELATINE_MODE_INF)
			DrawCercle(bd, bh, donnees->GetFloat(VONC_GELATINE_RAYON_MAX), Vector(0.663, 0.6, 1.));
	}
	else if (drawpass == DRAWPASS::HANDLES)
	{
		Int32			 i;
		Int32			 hitid = op->GetHighlightHandle(bd);
		HandleInfo info;

		bd->SetPen(GetViewColor(VIEWCOLOR_ACTIVEPOINT));
		bd->SetMatrix_Matrix(op, bh->GetMg());
		for (i = 0; i < PRISES; i++)
		{
			GetHandle(op, i, info);
			if (hitid == i)
				bd->SetPen(GetViewColor(VIEWCOLOR_SELECTION_PREVIEW));
			else
				bd->SetPen(GetViewColor(VIEWCOLOR_ACTIVEPOINT));
			bd->DrawHandle(info.position, DRAWHANDLE::BIG, 0);
		}

		GetHandle(op, 1, info);
		bd->SetPen(GetViewColor(VIEWCOLOR_ACTIVEPOINT));
		bd->DrawLine(info.position, Vector(0.0), 0);
	}
	return DRAWRESULT::OK;
}

void VoncGelatine::InitCourbe(BaseContainer* donnees) {
	AutoAlloc<SplineData> courbe;
	courbe->MakeLinearSplineBezier(2);
	CustomSplineKnot* knot0 = courbe->GetKnot(0);
	CustomSplineKnot* knot1 = courbe->GetKnot(1);
	knot0->vPos = Vector(0., 1., 0.);
	knot0->lFlagsSettings = 0;
	knot0->vTangentLeft = Vector(-.1, .4, 0.);
	knot0->vTangentRight = Vector(.1, -0.4, 0.);
	courbe->SetKnot(0, *knot0);
	knot1->vPos = Vector(1., 0., 0.);
	knot1->lFlagsSettings = 0;
	knot1->vTangentLeft = Vector(-.5, 0., 0.);
	knot1->vTangentRight = Vector(.5, 0., 0.);
	courbe->SetKnot(1, *knot1);
	donnees->SetData(VONC_GELATINE_ATTENUATION, GeData(CUSTOMDATATYPE_SPLINE, courbe));
}

Bool VoncGelatine::Init(GeListNode* node) {

	BaseObject*		 op = (BaseObject*)node;
	BaseContainer* donnees = op->GetDataInstance();

	InitCourbe(donnees);

	donnees->SetFloat(VONC_GELATINE_RAYON, rayon);
	donnees->SetFloat(VONC_GELATINE_RAYON_MAX, rayonMax);
	donnees->SetInt32(VONC_GELATINE_MODE, mode);
	donnees->SetFloat(VONC_GELATINE_INTENSITE, intensite);
	donnees->SetFloat(VONC_GELATINE_ELASTICITE, elasticite);
	donnees->SetFloat(VONC_GELATINE_AMORTIS, amortis);
	donnees->SetData(VONC_GELATINE_FORCE, force);
	donnees->SetBool(VONC_GELATINE_INVERSER, inverser);
	donnees->SetString(VONC_GELATINE_SELECTION, selection);
	donnees->SetFloat(VONC_GELATINE_ETIRE_ANGLE, angle);

	donnees->SetString(VONC_GELATINE_INFO, "v " + VERSION + " - code.vonc.fr");

	return true;
}

void VoncGelatine::Recup(BaseObject* op) {

	BaseContainer* donnees = op->GetDataInstance();

	rayon = donnees->GetFloat(VONC_GELATINE_RAYON);
	rayonMax = donnees->GetFloat(VONC_GELATINE_RAYON_MAX);
	mode = donnees->GetInt32(VONC_GELATINE_MODE);
	intensite = donnees->GetFloat(VONC_GELATINE_INTENSITE);
	elasticite = donnees->GetFloat(VONC_GELATINE_ELASTICITE);
	amortis = donnees->GetFloat(VONC_GELATINE_AMORTIS);
	force = donnees->GetFloat(VONC_GELATINE_FORCE);
	courbe = (SplineData*)(donnees->GetCustomDataType(VONC_GELATINE_ATTENUATION, CUSTOMDATATYPE_SPLINE));
	inverser = donnees->GetBool(VONC_GELATINE_INVERSER);
	selection = donnees->GetString(VONC_GELATINE_SELECTION);
	angle = donnees->GetFloat(VONC_GELATINE_ETIRE_ANGLE);

	donnees->SetString(VONC_GELATINE_INFO, "v " + VERSION + " - code.vonc.fr");

	if (rayonMax < rayon)
		rayonMax = rayon;
}

void VoncGelatine::CheckDirty(BaseObject* op, BaseDocument* doc) {
	
	Int32 tps = doc->GetTime().GetFrame(doc->GetFps());
	if (tps != tpsPrec)
	{
		tpsPrec = tps;
		op->SetDirty(DIRTYFLAGS::DATA);
	}
}

void VoncGelatine::RecupDepart(BaseObject* op, Matrix op_mg, Matrix mod_mg, vector<Vector>* departs) {

	departs->clear();

	if (mode == VONC_GELATINE_MODE_SRC)
	{
		departs->push_back(mod_mg.off);
	}
	else if (mode == VONC_GELATINE_MODE_SEL)
	{
		const Vector* points = ((PointObject*)op)->GetPointR();
		Int32 nbPoints = ((PointObject*)op)->GetPointCount();
		if (selection != "")
		{
			BaseTag* tag = op->GetFirstTag();
			while (tag)
			{
				if (tag->IsInstanceOf(Tpointselection) && tag->GetName() == selection)
				{
					BaseSelect* bs = ((SelectionTag*)tag)->GetBaseSelect();
					if (bs)
					{
						Int32 seg = 0, a, b, i;
						while (bs->GetRange(seg++, nbPoints, &a, &b))
						{
							for (i = a; i <= b; ++i)
							{
								departs->push_back(op_mg * points[i]);
							}
						}
					}
					break;
				}
				tag = tag->GetNext();
			}
		}
	}
}

vector<Float> VoncGelatine::CalculeFacteur(PointObject* op, Matrix op_mg, Matrix mod_mg) {

	vector<Float> pointsFacteur;

	Int32 nbPoints = op->GetPointCount();
	const Vector* points = op->GetPointR();
	
	if (mode == VONC_GELATINE_MODE_INF)
	{
		pointsFacteur = vector<Float>(nbPoints, 1.);
		return pointsFacteur;
	}

	Float distanceMax = rayonMax - rayon;

	pointsFacteur = vector<Float>(nbPoints, distanceMax * 10.);
	vector<Vector> departs = vector<Vector>();

	RecupDepart(op, op_mg, mod_mg, &departs);

	for (vector<Vector>::iterator pointDepart = departs.begin(); pointDepart != departs.end(); ++pointDepart)
	{

		Vector point;
		Float distance;

		for (Int32 i = 0 ; i < nbPoints ; i++)
		{

			point = op_mg * points[i];
			distance = ((*pointDepart) - point).GetLength() - rayon;
			if (distance < 0.)
				distance = 0.;
			if (pointsFacteur[i] > distance)
				pointsFacteur[i] = distance;
		}
	}

	if (distanceMax > 0.)
	{
		Int32 i = 0;
		Float pfac;
		for (vector<Float>::iterator fac = pointsFacteur.begin(); fac != pointsFacteur.end(); ++fac)
		{
			pfac = (*fac) / distanceMax;
			if (pfac > 1.)
				pfac = 1.;
			(*fac) = pfac;
			i++;
		}
	}

	return pointsFacteur;
}

vector<Vector> VoncGelatine::CalculeNormales(PolygonObject* op, Matrix op_mg) {
	Int32 nbPolys = ((PolygonObject*)op)->GetPolygonCount();
	Int32 nbPoints = ((PolygonObject*)op)->GetPointCount();
	vector<Vector> normalesPoints = vector<Vector>(nbPoints, Vector());
	const CPolygon* polygones = op->GetPolygonR();
	const Vector* points = op->GetPointR();
	CPolygon poly;
	Vector normale;
	op_mg.off = Vector();

	for (Int32 i = 0; i < nbPolys; i++)
	{
		poly = polygones[i];
		normale = Cross(points[poly.a] - points[poly.c], points[poly.b] - points[poly.d]);
		normale = op_mg * normale;
		normalesPoints[poly.a] += normale;
		normalesPoints[poly.b] += normale;
		normalesPoints[poly.c] += normale;
		if (poly.c != poly.d)
			normalesPoints[poly.d] += normale;
	}

	return normalesPoints;
}

String VoncGelatine::ObjID(BaseObject* op) {
	String ip = String::IntToString(op->GetUniqueIP());
	String guid = String::UIntToString(op->GetGUID());
	return ip + guid + op->GetName();
}

void VoncGelatine::Libere(BaseObject* mod) {
	for (map<String, Cache>::iterator it = pointsCache.begin(); it != pointsCache.end(); ++it)
	{
		it->second.precAbs.clear();
		it->second.elasticite.clear();
		it->second.facteur.clear();
	}
	pointsCache.clear();
	//GePrint("Lib�re");
}

void VoncGelatine::Initialise(PointObject* op, Matrix op_mg, Matrix mod_mg, String id) {

	Int32 nbPoints = op->GetPointCount();
	vector<Vector> pointsElasticite = vector<Vector>(nbPoints, Vector());
	vector<Vector> pointsPrecAbs = vector<Vector>(nbPoints, Vector());
	const Vector* points = op->GetPointR();

	for (Int32 i = 0; i < nbPoints; i++)
	{
		pointsPrecAbs[i] = op_mg * points[i];
	}

	vector<Float> pointsFacteur = CalculeFacteur(op, op_mg, mod_mg);

	Cache cache;
	cache.precAbs = pointsPrecAbs;
	cache.elasticite = pointsElasticite;
	cache.facteur = pointsFacteur;
	cache.secPrec = -1.;

	//pointsCache.insert(pair<String, Cache>(id, cache));
	pointsCache[id] = cache;

	//GePrint("Init " + id);
}

void VoncGelatine::RendUniqueOpsIter(BaseObject* obj, vector<Int32>* ips) {
	if (!obj)
		return;

	Int32 ip = obj->GetUniqueIP();
	
	while (find(ips->begin(), ips->end(), ip) != ips->end())
	{
		ip++;
	}

	obj->SetUniqueIP(ip);
	ips->push_back(ip);

	// Met � jour la propri�t� s�lection de points
	if (mode == VONC_GELATINE_MODE_SEL)
	{
		BaseTag* tag = obj->GetFirstTag();
		while (tag)
		{
			if (tag->IsInstanceOf(Tpointselection) && tag->GetName() == selection)
			{
				tag->SetDirty(DIRTYFLAGS::DATA);
			}
			tag = tag->GetNext();
		}
	}

	RendUniqueOpsIter(obj->GetDown(), ips);
	RendUniqueOpsIter(obj->GetNext(), ips);
}

void VoncGelatine::RendUniqueOps(BaseObject* mod) {
	vector<Int32> ips;
	RendUniqueOpsIter(mod->GetUp(), &ips);
}

Bool VoncGelatine::DoitInit(BaseObject* op, String id) {
	if (pointsCache.find(id) == pointsCache.end())
		return true;
	if (pointsCache[id].precAbs.size() != ((PointObject*)op)->GetPointCount())
		return true;
	return false;
}

Bool VoncGelatine::DoitLiberer(BaseTime* time, BaseDocument* doc) {
	Bool libere = false;
	Float tps = time->GetFrame(doc->GetFps());

	// Si on est � l'image 0, lib�rer la m�moire une fois
	if (tps != ansTps && abs(tps) <= 0.000001)
		libere = true;

	// Si certains param�tres ont chang�, lib�rer la m�moire
	if (ansRayon != rayon)
		libere = true;
	if (ansRayonMax != rayonMax)
		libere = true;
	if (ansMode != mode)
		libere = true;
	if (ansSelection != selection)
		libere = true;

	ansTps = tps;
	ansRayon = rayon;
	ansRayonMax = rayonMax;
	ansMode = mode;
	ansSelection = selection;

	return libere;
}

Bool VoncGelatine::ModifyObject(BaseObject* mod, BaseDocument* doc, BaseObject* op, const Matrix& op_mg, const Matrix& mod_mg, Float lod, Int32 flags, BaseThread* thread)
{

	// Si pas de points, terminer
	if (!op->IsInstanceOf(Opoint))
		return true;

	Int32 nbPoints = ((PointObject*)op)->GetPointCount();
	if (nbPoints == 0)
		return true;

	// R�cup�re les param�tres
	Recup(mod);

	// ID de l'objet en cours de traitement
	String id = ObjID(op);

	// Variables de temps
	BaseTime time = doc->GetTime();
	Float sec = time.Get();

	// Lib�re la m�moire ?
	if (DoitLiberer(&time, doc))
	{
		Libere(mod);
		RendUniqueOps(mod);
	}

	// Si la m�moire est vide ou si op a chang�, initialiser ses tableaux de points
	if (DoitInit(op, id))
	{
		Initialise((PointObject*)op, op_mg, mod_mg, id);
	}

	// Bool si le temps est pass� ou non
	Bool tpsPasse = sec != pointsCache[id].secPrec;

	
	//------------------------------------------------------------------------------//


	// Variables de traitement
	Vector* points = ((PointObject*)op)->GetPointW();
	Float32* weight = ((PointObject*)op)->CalcVertexMap(mod);
	vector<Vector>* pointsPrecAbs = &(pointsCache[id].precAbs);
	vector<Vector>* pointsElasticite = &(pointsCache[id].elasticite);
	vector<Float>* pointsFacteur = &(pointsCache[id].facteur);
	Matrix op_mg_inv = ~op_mg;
	Vector point;
	Vector pointPrecAbs;
	Vector pointAbs;
	Vector pointElasticite;
	Vector pointNouveauAbsFac;
	Vector pointNouveau;
	Float fac;
	Float facVitesse;
	Float facAmortissement;
	vector<Vector> normalesPoints;
	Vector direction;
	Vector normale;
	Float facNormale;
	Bool utilAngleBool = angle > 0.00001 && (3.14159265359 - angle) > 0.00001;
	
	// Calcul des normales
	if (utilAngleBool)
		normalesPoints = CalculeNormales((PolygonObject*)op, op_mg);

	// Calcul des nouveaux points
	for (Int32 i = 0; i < nbPoints; i++)
	{

		point = points[i];
		pointPrecAbs = (*pointsPrecAbs)[i];
		pointAbs = op_mg * point;

		fac = (*pointsFacteur)[i];
		if (weight)
			fac *= 1. - weight[i];
		if (inverser)
			fac = 1. - fac;

		if (courbe)
		{
			facVitesse = courbe->GetPoint(fac * elasticite).y;
			facAmortissement = courbe->GetPoint(fac * amortis).y;
		}
		else
		{
			facVitesse = 1 - (fac * elasticite);
			facAmortissement = 1. - (fac * amortis);
		}

		pointElasticite = (*pointsElasticite)[i] * (1. - facVitesse);
		pointElasticite += (pointAbs - pointPrecAbs) * ClampValue(force + facVitesse, 0., 1.);

		// Angle d'amortissement
		if (utilAngleBool)
		{
			direction = pointElasticite.GetNormalized();
			normale = normalesPoints[i].GetNormalized();
			facNormale = GetAngle(direction, normale) / (3.14159265359 - angle);
			facNormale = Clamp01(facNormale);
			facAmortissement = 1. - (facNormale * (1. - facAmortissement));
			if (facAmortissement < 0.)
				facAmortissement = 0.;
		}

		pointNouveauAbsFac = pointElasticite * facAmortissement;

		pointPrecAbs = (*pointsPrecAbs)[i] + pointNouveauAbsFac;

		if (tpsPasse)
		{
			(*pointsElasticite)[i] = pointElasticite;
			(*pointsPrecAbs)[i] = pointPrecAbs;
		}

		pointNouveau = op_mg_inv * pointPrecAbs;

		// Intensit�
		pointNouveau = MixVec(point, pointNouveau, intensite);

		// D�finit le nouveau point
		points[i] = pointNouveau;
	}

	DeleteMem(weight);
	normalesPoints.clear();

	op->Message(MSG_UPDATE);

	// Variables pr�c�dentes
	pointsCache[id].secPrec = sec;

	return true;
}

Bool VoncGelatine::Message(GeListNode* node, Int32 type, void* data)
{
	if (type == MSG_MENUPREPARE)
	{
		((BaseObject*)node)->SetDeformMode(true);
	}
	else if (type == MSG_DESCRIPTION_COMMAND)
	{
		BaseContainer* donnees = ((BaseObject*)node)->GetDataInstance();
		DescriptionCommand* dc = (DescriptionCommand*)data;
		const Int32 id = dc->_descId[0].id;

		switch (id)
		{

			case VONC_GELATINE_ATTENUATION_INIT :
				InitCourbe(donnees);
				break;

			case VONC_GELATINE_INIT :
				Libere((BaseObject*)node);
				RendUniqueOps((BaseObject*)node);
				((BaseObject*)node)->SetDirty(DIRTYFLAGS::DATA);
				break;

			default:
				break;
		}
	}
	return true;
}

Bool VoncGelatine::GetDEnabling(GeListNode* node, const DescID &id, const GeData &t_data, DESCFLAGS_ENABLE flags, const BaseContainer* itemdesc) {

	BaseContainer* donnees = ((BaseObject*)node)->GetDataInstance();
	
	if (donnees)
	{
		
		Int32 mode = donnees->GetInt32(VONC_GELATINE_MODE);

		switch (id[0].id)
		{

			case VONC_GELATINE_RAYON :
				if (mode != VONC_GELATINE_MODE_SRC)
					return false;
				break;

			case VONC_GELATINE_RAYON_MAX :
				if (mode == VONC_GELATINE_MODE_INF)
					return false;
				break;

			case VONC_GELATINE_SELECTION :
				if (mode != VONC_GELATINE_MODE_SEL)
					return false;
				break;

			default :
				break;
		}

	}
	
	return ObjectData::GetDEnabling(node, id, t_data, flags, itemdesc);
}


Bool EnregistreVoncGelatine()
{
	return RegisterObjectPlugin(MODULE_ID, GeLoadString(VONC_GELATINE_NOM), OBJECT_MODIFIER, VoncGelatine::Alloc, "Ovoncgelatine"_s, AutoBitmap("Ovoncgelatine.tif"_s), 0);
}
